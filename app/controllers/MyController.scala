package controllers

import javax.inject._
import play.api.mvc._
import models.JarHandeller.{runJarMethod, runSparkJarMethod}
import org.apache.spark.sql.DataFrame

import java.io.File
import java.nio.file.{Files, Paths}
import scala.concurrent._
import ExecutionContext.Implicits.global

//curl --header "Content-Type: application/json" --request POST --data "{"""sparktoggle""":true,"""jarpath""":"""D:/Scala/JarPOC/SparkJarPOC/target/scala-2.12/spark_jar_poc_assembly.jar""","""classname""":"""com.kesiee.POC""","""methodname""":"""display"""}" --output "result.txt" page.html http://localhost:9000/test

//"{"""sparktoggle""":true,"""jarpath""":"""D:/Scala/JarPOC/SparkJarPOC/target/scala-2.12/spark_jar_poc_assembly.jar""","""classname""":"""com.kesiee.POC""","""methodname""":"""display"""}"

@Singleton
class MyController @Inject()(cc: ControllerComponents) extends AbstractController(cc) {

  def myAction = Action(parse.json)  { implicit request=>
    val jsonBody = request.body

    val sparkFlag= jsonBody.\("sparktoggle").get.as[Boolean]
    val jarFilePath = jsonBody.\("jarpath").get.as[String]
    val className = jsonBody.\("classname").get.as[String]
    val methodName = jsonBody.\("methodname").get.as[String]

    if (sparkFlag){
      // can be a json string and flatten to map
      val sparkParams = Map("spark.master" -> "local[*]", "spark.app.name" -> "My App")
      val result = runSparkJarMethod(jarFilePath, className, methodName, sparkParams)

      val result_string=result.asInstanceOf[DataFrame].toJSON.collect().mkString(",")
      val filename = "example.txt"
      val fileContent = result_string.getBytes("UTF-8")
      val file = File.createTempFile("temp-", ".tmp")
      Files.write(file.toPath, fileContent)

      val headers = Map(
        CONTENT_DISPOSITION -> s"""attachment; filename="$filename"""",
        CONTENT_TYPE -> "application/octet-stream",
        CONTENT_LENGTH -> file.length.toString
      )

      Ok.sendFile(file, inline = false, fileName = _  => Option(filename)).withHeaders(headers.toSeq: _*)

    }

    else{
      val result = runJarMethod(jarFilePath, className, methodName)
      Ok(result.toString)
    }
  }

}





