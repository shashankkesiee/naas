package models

import com.amazonaws.auth.{AWSStaticCredentialsProvider, BasicAWSCredentials}
import com.amazonaws.regions.Regions
import com.amazonaws.services.s3.AmazonS3ClientBuilder
import org.apache.spark.sql.SparkSession

object Configurations {

  val ACCESS_KEY = "AKIAQU6NAOBVBAXRE3UI"
  val SECRET_KEY = "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"

  implicit val spark = SparkSession.builder()
    .master("local[*]")
    .config("spark.hadoop.fs.s3a.access.key", ACCESS_KEY)
    .config("spark.hadoop.fs.s3a.secret.key", SECRET_KEY)
    .config("spark.hadoop.fs.s3a.impl", "org.apache.hadoop.fs.s3a.S3AFileSystem")
    .config("spark.hadoop.fs.s3a.aws.credentials.provider", "org.apache.hadoop.fs.s3a.SimpleAWSCredentialsProvider")
    .config("spark.hadoop.fs.s3a.endpoint", "s3.amazonaws.com")
    .getOrCreate();

  val region = Regions.US_EAST_1
  val bucketName = "api-poc-kesiee"
  val key = "jars/SparkJarPOC.jar"
  val localFilePath = s"D:/Scala/playPOC/$key"

  val credentials = new BasicAWSCredentials(ACCESS_KEY, SECRET_KEY)
  val s3Client = AmazonS3ClientBuilder.standard()
    .withCredentials(new AWSStaticCredentialsProvider(credentials))
    .withRegion(region)
    .build()
}
