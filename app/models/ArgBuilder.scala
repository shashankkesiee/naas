package models

import org.apache.spark.sql.SparkSession
import scala.collection.mutable.ListBuffer

object ArgBuilder {
  val data_mapper = Map(
    "string" -> classOf[String],
    "integer" -> classOf[Int],
    "boolean" -> classOf[Boolean],
    "sparksession" -> classOf[SparkSession])

  def arg_builder(datatype: List[String]): Seq[Class[_]] = {
    val args = new ListBuffer[Class[_]]()
    for (x <- datatype) {
      args += data_mapper(x)
    }
    args.toSeq
  }

}
