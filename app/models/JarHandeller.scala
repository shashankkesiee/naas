package models

import models.Configurations.{ACCESS_KEY, SECRET_KEY, bucketName, key, localFilePath, s3Client, spark}
import com.amazonaws.auth.{AWSStaticCredentialsProvider, BasicAWSCredentials}
import com.amazonaws.regions.Regions
import com.amazonaws.services.s3.AmazonS3ClientBuilder
import com.amazonaws.services.s3.model.GetObjectRequest
import models.ArgBuilder.arg_builder
import org.apache.spark.SparkConf
import org.apache.spark.sql.{DataFrame, SparkSession}

import java.io.File
import java.net.URLClassLoader

object JarHandeller{
  def runSparkJarMethod(jarFilePath: String, className: String, methodName: String, sparkParams: Map[String, String]) = {
    val jarClassLoader = new URLClassLoader(Array(new File(jarFilePath).toURI.toURL), getClass.getClassLoader)
    val cls = jarClassLoader.loadClass(className)


    val datatype_list=List("sparksession","string")
    val args= arg_builder(datatype_list)
    val method = cls.getDeclaredMethod(methodName, args:_*)

    val sparkConf = new SparkConf().setAppName("Jar Method Runner")
    for ((param, value) <- sparkParams) {
      sparkConf.set(param, value)
    }

    val spark= SparkSession.builder()
      .config(sparkConf)
      .getOrCreate()

    method.invoke(null, spark, "C:/Users/Shashank.K.ACS/Downloads/unece (1).json")
  }

  def runJarMethod(jarFilePath: String, className: String, methodName: String) = {
    val jarClassLoader = new URLClassLoader(Array(new File(jarFilePath).toURI.toURL), getClass.getClassLoader)
    val cls = jarClassLoader.loadClass(className)
    val method = cls.getDeclaredMethod(methodName)
    method.invoke(null)
  }

}
