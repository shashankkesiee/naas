name := """NAAS"""
organization := "com.kesiee"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.12.10"


libraryDependencies += "org.scalatestplus.play" %% "scalatestplus-play" % "5.0.0" % Test
libraryDependencies += guice
libraryDependencies += "com.amazonaws" % "aws-java-sdk" % "1.12.441"
libraryDependencies += "org.apache.spark" %% "spark-core" % "3.0.1"
libraryDependencies += "org.apache.spark" %% "spark-sql" % "3.0.1"
libraryDependencies += "com.fasterxml.jackson.module" %% "jackson-module-scala" % "2.12.1"

// Adds additional packages into Twirl
//TwirlKeys.templateImports += "com.kesiee.controllers._"
//
//// Adds additional packages into conf/routes
// play.sbt.routes.RoutesKeys.routesImport += "com.kesiee.binders._"
